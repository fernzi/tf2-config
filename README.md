# TF2 Configs

My configuration files for Team Fortress 2. Made for a 5 button mouse.

## Installation

Clone or extract a downloaded zip either into your `cfg` folder, or into the
`cfg` of a folder inside your `custom` directory, if you'd rather not pollute
your existing configs.

Remember to backup your `config.cfg` file or set it to read-only in case you
ever want to return to your previous configurations.

## Usage

Rather than scrolling through weapons, each button is configured to always
perform a specific action for each class: e.g. for Medic, <kbd>Mouse1</kbd>
heals, <kbd>Mouse2</kbd> fires the crossbow, <kbd>Mouse3</kbd> strikes with the
saw, <kbd>Mouse4</kbd> activates uber, etc.

The only real constant is that <kbd>Mouse3</kbd>, this is, middle click, is
always set to melee, so see each individual config file for more details.

Additionally, the configs also include graphical optimizations, mouse settings,
a null movement script and network settings more appropiate for a world that's
not stuck in 2007.
